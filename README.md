# pasctri

Calculate Pascal's Triangle up to a certain number, n. Because of number limitations this program should not be used for n > 1000.

Usage: pasctri.py n

