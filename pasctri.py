#! /usr/bin/env python3

# Usage: pasctri.py n
# n is the desired row number
# pasctri will then output all lines from 0 up to the specified row number
# Note that this program is rather simple and can't handle too large numbers, ideally use this for n <= 1000

import sys

# Calculates row number n and returns it as a list
def calculate_row(n):
    a = n
    b = 1
    row = [1]
    while a > 0:
        row.append(int(round(row[b-1]*(a/b))))
        a -= 1
        b += 1
    return row

#main
if len(sys.argv) != 2:
    sys.exit("Error: Missing required row number.")
if not sys.argv[1].isdigit() or int(sys.argv[1]) < 0:
    sys.exit("Error: Argument must be a positive integer")

n = 0
while n <= int(sys.argv[1]):
    print(*calculate_row(n), sep=" ")
    n += 1
